<?php
include_once ('../../vendor/autoload.php');
use AbuSaleh\Utility\Utility;

$obj3 = new Utility();

use AbuSaleh\Gender\Gender;
$obj4 = new Gender();
$data = $obj4->prepare($_GET)->show();
//$obj3->debug($data);
?>

<table border="1" cellpadding="5">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Gender</th>
    </tr>
    <tr>
        <th><?php echo $data['id']; ?></th>
        <th><?php echo ucwords($data['name']); ?></th>
        <th><?php echo ucfirst($data['gender']); ?></th>
    </tr>
</table>
<a href="index.php">View All</a>

