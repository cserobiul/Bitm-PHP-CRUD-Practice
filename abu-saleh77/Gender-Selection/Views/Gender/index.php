<h2>All List</h2>
<script>
    function delchk() {
        return confirm("Are you sure to delete??");
    }
</script>
<?php
include_once ('../../vendor/autoload.php');
use AbuSaleh\Gender\Gender;
$obj2 = new Gender();
$data = $obj2->index();
?>
<?php
if(isset($_SESSION['msg']) && !empty($_SESSION['msg'])){
    echo $_SESSION['msg'];
    unset($_SESSION['msg']);
}
?>
<table border="1" cellpadding="5">
    <tr>
        <th>Name</th>
        <th>Gender</th>
        <th colspan="3">Action</th>
    </tr>
    <?php
        foreach ($data as $item){ ?>
            <tr>
                <td><?php echo ucwords($item['name']); ?></td>
                <td><?php echo ucfirst($item['gender']); ?></td>
                <td><a href="show.php?id=<?php echo $item['id']; ?>">View</a></td>
                <td><a href="edit.php?id=<?php echo $item['id']; ?>">Edit</a></td>
                <td><a href="delete.php?id=<?php echo $item['id']; ?>" onclick="return delchk()">Delete</a></td>
            </tr>
    <?php } ?>
</table>
<a href="create.php">Add New</a>
