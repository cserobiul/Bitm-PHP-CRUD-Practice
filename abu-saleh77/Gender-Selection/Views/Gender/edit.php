<?php
include_once ('../../vendor/autoload.php');
$obj4 = new AbuSaleh\Gender\Gender();
$obj5 = new AbuSaleh\Utility\Utility();
//$obj5->debug($_GET);
$_SESSION['id'] = $_GET['id'];
$data = $obj4->prepare($_GET)->show();
?>

<fieldset>
    <legend>Update Gender Selection</legend>
    <form action="update.php" method="POST">
        <label>Name: </label>
        <input type="text" name="name" value="<?php echo $data['name']; ?>">
        <label>Gender</label>
        <?php
        if($data['gender'] == 'male'){ ?>
            <input type="radio" name="gender" value="male"checked>Male
            <input type="radio" name="gender" value="female">Female
       <?php }elseif($data['gender'] == 'female'){ ?>
            <input type="radio" name="gender" value="male">Male
            <input type="radio" name="gender" value="female" checked>Female
        <?php }else{ ?>
            <input type="radio" name="gender" value="male">Male
        <input type="radio" name="gender" value="female">Female
       <?php }
        ?>

        <input type="submit" value="Click for Update">
    </form>
    <a href="index.php">View All</a>
</fieldset>
