<?php
namespace App\LaptopModel;
use PDO;
class Laptop
{
    public $id = "";
    public $name = "";
    public $brand = "";
    public $data = "";
    public $host = "localhost";
    public $db = "php27";
    public $user = "root";
    public $pass = "";
    public $conn = "";

    public function __construct()
    {
        session_start();
        $this->conn = new PDO('mysql:host=$this->host;dbname=$this->db', $this->user, $this->pass);
    }

    public function assign($data = '')
    {
        if (!empty($data['laptop_name'])) {
            $this->name = $data['laptop_name'];
        } else {
            $_SESSION['laptop_name'] = "Required Laptop Name";
        }

        if (!empty($data['laptop_brand'])) {
            $this->brand = $data['laptop_brand'];
        } else {
            $_SESSION['laptop_brand'] = "Required Laptop Brand";
        }

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }

        return $this;
    }

    public function store()
    {
        if(!empty($this->name) && !empty($this->brand)){

            try{
                $qry = "INSERT INTO laptop (name, brand, u_id) VALUES (:name, :brand, u_id)";
                $q = $this->conn->prepare($qry);
                $q->execute(array(
                    ':name' => $this->name,
                    ':brand' => $this->brand,
                    ':u_id' => uniqid()
                ));
                $_SESSION['msg'] = "Successfully Submitted";
                header("location: index.php");
            }catch (PDOException $e){
                echo 'Error: '. $e->getMessage();
            }

        }else{
            header("location: create.php");
        }

        return $this;
    }

}