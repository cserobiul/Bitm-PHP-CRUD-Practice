<?php
include_once('../vendor/mpdf/mpdf/mpdf.php');
include_once('../vendor/autoload.php');

$pdfObj = new \App\Terms();
$data = $pdfObj->index();
$rows = "";
$serial = 0;
foreach ($data as $items):
    $serial++;
    $rows .= "<tr>";
    $rows .= "<td>" . $serial . "</td>";
    $rows .= "<td>" . $items['name'] . "</td>";
    $rows .= "<td>" . $items['tc'] . "</td>";
    $rows .= "</tr>";
endforeach;
$html = <<<EOF
<h2>List Of Data</h2>
<table border="1">
    <tr>
        <th>SL</th>
        <th>Name</th>
        <th>Terms</th>
    </tr>
    $rows;
</table>
EOF;

$mpdf = new mPDF();
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;



























