<?php
session_start();

?>

<html>
<head>
    <title>LaptopModel :: Create</title>
</head>
<body>
<form action="store.php" method="POST">
    <fieldset>
        <legend>Laptop Model</legend>
        <lable>Laptop Name</lable>
        <input type="text" name="laptop_name" autofocus>
        <?php
        if (isset($_SESSION['laptop_name']) && !empty($_SESSION['laptop_name'])) {
            echo $_SESSION['laptop_name'];
            unset($_SESSION['laptop_name']);
        }
        ?>
        <br>
        <label for="">Laptop Brand</label>
        <select name="laptop_brand" id="">
            <option value="">Select One</option>
            <option value="hp">HP</option>
            <option value="dell">Dell</option>
            <option value="sony">Sony</option>
        </select>
        <?php
        if (isset($_SESSION['laptop_brand']) && !empty($_SESSION['laptop_brand'])) {
            echo $_SESSION['laptop_brand'];
            unset($_SESSION['laptop_brand']);
        }
        ?>
        <br>
        <input type="submit" value="Add Laptop">
    </fieldset>
</form>
</body>
</html>
