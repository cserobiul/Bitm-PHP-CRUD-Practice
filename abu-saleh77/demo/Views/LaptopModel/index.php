<a href="create.php">Add New</a>
<?php
include_once ('../../vendor/autoload.php');

$obj = new App\LaptopModel\LaptopModel();

if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
    echo "<h4>" . $_SESSION['msg'] . "</h4>";
    unset($_SESSION['msg']);
}