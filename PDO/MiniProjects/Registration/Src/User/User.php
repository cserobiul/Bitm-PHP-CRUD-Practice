<?php
namespace Apps\User;

use PDO;
use PDOException;

class User
{
    public $id = "";
    public $username = "";
    public $email = "";
    public $pass = "";
    public $rpass = "";
    public $tc = "";
    public $is_admin = "";
    public $is_active = "";
    public $data = "";
    public $today = "";
    public $conn = "";

    public function __construct()
    {
        session_start();
        date_default_timezone_set("Asia/Dhaka");
        $this->today = date("Y-m-d G:i:s");
        //echo 'Working';
        try {
            $this->conn = new PDO('mysql:host=localhost;dbname=bitmphp;', "root", "");
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    // value assign
    public function assign($data = '')
    {
        // check id
        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }

        // check username
        if (!empty($data['username'])) {
            $this->username = $data['username'];

        } else {
            $_SESSION['usernameMsg'] = "Username is Required";
        }

        // check email

        if (!empty($data['email'])) {
            $this->email = $data['email'];
        } else {
            $_SESSION['emailMsg'] = "Email is Required";
        }

        // check user role
        if (!empty($data['userRole'])) {
            if ($data['userRole'] == 'admin') {
                $this->is_admin = 1;
            } else {
                $this->is_admin = 0;
            }
        } else {
            $_SESSION['userRoleMsg'] = "User Role is Required";
        }

        // check user Status

        if (!empty($data['userStatus'])) {
            if ($data['userStatus'] == 'active') {
                $this->is_active = 1;
            } else {
                $this->is_active = 0;
            }
        } else {
            $_SESSION['userStatusMsg'] = "User Status is Required";
        }

        // check password
        if (!empty($data['password'])) {
            $this->pass = $data['password'];
        } else {
            $_SESSION['passwordMsg'] = "Password is Required";
        }

        // check repeated password
        if (!empty($data['rpassword'])) {
            $this->rpass = $data['rpassword'];
        } else {
            $_SESSION['rpasswordMsg'] = "Repeated Password is Required";
        }


        return $this;

    }

    public function checkUsername()
    {
        try {
            $chkQuery = "SELECT * FROM users WHERE username = :u ";
            $q = $this->conn->prepare($chkQuery) or die("Unable to Query");
            $q->execute(array(
                ':u' => $this->username,
            ));
            $rowCount = $q->rowCount();
            return $rowCount;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

        return $this;
    }

    //check email

    public function checkEmail()
    {
        try {
            $chkQuery = "SELECT * FROM users WHERE email = :e ";
            $q = $this->conn->prepare($chkQuery) or die("Unable to Query");
            $q->execute(array(
                ':e' => $this->email,
            ));
            $rowCount = $q->rowCount();
            return $rowCount;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

        return $this;
    }

    // get active user data
    public function index()
    {
        try {
            $Query = "SELECT * FROM users WHERE is_active = :a";
            $q = $this->conn->prepare($Query) or die('Unable to Query');
            $q->execute(array(
                ':a' => $this->is_active,
            ));

            $rowCount = $q->rowCount();
            if ($rowCount > 0) {
                while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                    $data[] = $row;
                }
                return $data;
            } else {
                return $rowCount;
            }


        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        //return $this;
    }


    // get single user

    public function show()
    {
        try {
            $Query = "SELECT * FROM users WHERE u_id = :uid";
            $q = $this->conn->prepare($Query) or die('Unable to Query');
            $q->execute(array(
                ':uid' => $this->id,
            ));
            $rowCount = $q->rowCount();
            if ($rowCount > 0) {
                $data = $q->fetch(PDO::FETCH_ASSOC);
                return $data;
            }else{
                return $rowCount;
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this;
    }


    // data store

    public function store()
    {
        try {
            $query = "INSERT INTO users(username,email,pass,tc,u_id,is_active,is_admin,created_at) VALUES(:u,:e,:p,:tc,:uID,:is_active,:is_admin,:created_at)";
            $q = $this->conn->prepare($query);
            $q->execute(array(
                ':u' => $this->username,
                ':e' => $this->email,
                ':p' => $this->pass,
                ':tc' => '1',
                ':uID' => uniqid(),
                ':is_active' => $this->is_active,
                ':is_admin' => $this->is_admin,
                ':created_at' => date("Y-m-d G:i:s"),
            ));
            $row = $q->rowCount();
            if($row>0){
                $id = $this->conn->lastInsertId();
                $query = "INSERT INTO profiles(user_id) VALUES(:id)";
                $qr = $this->conn->prepare($query);
                $qr->execute(array(
                    ':id' => $id,
                ));
                $rowCount = $qr->rowCount();
                return $rowCount;
            }else{
                return $row;
            }


        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this;
    }


// user login
    public function login()
    {
        try {
            $loginQuery = "SELECT * FROM users WHERE username = :u && pass = :p && is_active = :ac ";
            $q = $this->conn->prepare($loginQuery) or die("Unable to Query");
            $q->execute(array(
                ':u' => $this->username,
                ':p' => $this->pass,
                ':ac' => '1',
            ));
            $rowCount = $q->rowCount();
            if ($rowCount > 0) {
                $row = $q->fetch(PDO::FETCH_ASSOC);
                return $row;
            } else {
                $_SESSION['logMsg'] = "Invalid Information or <br> Your account is not Active";
                header("location:login.php");
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

        return $this;
    }


}