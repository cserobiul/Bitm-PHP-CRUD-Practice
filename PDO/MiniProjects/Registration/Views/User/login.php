<?php
include_once('../../vendor/autoload.php');
include_once('layout/header.php');
$dataObj = new \Apps\User\User();
if (isset($_SESSION['loginUser']) && !empty($_SESSION['loginUser'])) {

    header("location:index.php");
}
?>
    <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
        <form class="form-horizontal" action="loginProcess.php" method="POST">
            <div class="form-group">
                <label for="username" class="col-md-2 control-label">Username</label>
                <div class="col-md-6">
                    <input type="text" class="form-control" name="username" id="username" placeholder="Username">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-md-2 control-label">Password</label>
                <div class="col-md-6">
                    <input type="password" class="form-control" name="password" id="password" placeholder="********">
                </div>
            </div>

            <?php
            if (isset($_SESSION['logMsg']) && !empty($_SESSION['logMsg'])) { ?>
                <div class="form-group">
                    <label for="" class="col-md-2 control-label"></label>
                    <div class="col-md-6">

                            <?php
                            echo '<strong><font color="#b22222">' . $_SESSION['logMsg'] . '</font></strong>';
                            unset($_SESSION['logMsg']);
                            ?>

                    </div>
                </div>
            <?php } ?>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-4">
                    <button type="submit" class="btn btn-success">
                        <a href="login.php"> <span class="glyphicon glyphicon-log-in" aria-hidden="true"> </span> Sign
                            in</a></button>
                    <a href="create.php">  Sign
                        Up</a>
                </div>
            </div>
        </form>
    </div>


<?php include_once('layout/footer.php'); ?>