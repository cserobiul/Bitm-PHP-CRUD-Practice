<?php
include_once('../../vendor/autoload.php');
$dataObj = new \Apps\User\User();
include_once('layout/header.php');
include_once 'checkLogin.php';


$data = $dataObj->assign($_GET)->show();

if (!empty($data)) { ?>
    <div class="panel col-md-6 col-md-offset-3">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                    Details about <b><?php echo ucwords($data['username']); ?></b></h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover">
                        <thead></thead>
                        <tbody>
                        <tr>
                            <th>Username</th>
                            <td><?php echo ucwords($data['username']); ?></td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td><?php echo $data['email']; ?></td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td><?php
                                if ($data['is_active'] == 1) {
                                    echo 'Active';
                                } else {
                                    echo 'Deactive';
                                } ?>
                            </td>
                        </tr>
                        <tr>
                            <th>User Type</th>
                            <td><?php
                                if ($data['is_admin'] == 1) {
                                    echo 'Admin';
                                } else {
                                    echo 'User';
                                } ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Join at</th>
                            <td><?php echo $data['created_at']; ?></td>
                        </tr>
                        <tr>
                            <td class="text-center" colspan="2">
                                <a href="index.php" class="btn btn-default">
                                    <span class="glyphicon glyphicon-dashboard" aria-hidden="true"></span>
                                    Back to Dashboard</a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php } else {
    $_SESSION['errMsg'] = "You are Unauthorized user";
    header('location:404.php');
}
?>

<?php include_once('layout/footer.php'); ?>