<?php
include_once('../../vendor/autoload.php');
$storeObj = new \Apps\User\User();


echo '<pre>';
print_r($_REQUEST);

if (!empty($_REQUEST['tc'])) {

    $chkUser = $storeObj->assign($_REQUEST)->checkUsername();

    $chkEmail = $storeObj->assign($_REQUEST)->checkEmail();

// check user
    if ($chkUser == 0) {
        if ((strlen(trim($_REQUEST['username']))) >= 4 && (strlen(trim($_REQUEST['username'])) <= 15)) {
            $_SESSION['usernameValue'] = $_REQUEST['username'];
        } else {
            $_SESSION['usernameMsg'] = "Username Must be 4 to 15";
            unset($_REQUEST['username']);
        }
    } else {
        $_SESSION['usernameMsg'] = "Username Already Exist";
        unset($_REQUEST['username']);
    }

// check email
    if ($chkEmail == 0) {
        $_SESSION['emailValue'] = $_REQUEST['email'];
    } else {
        $_SESSION['emailMsg'] = "Email Already Exist";
        unset($_REQUEST['email']);
    }

// check password

    if (strlen(trim($_REQUEST['password'])) >= 6 && strlen(trim($_REQUEST['password'])) <= 10 &&
        strlen(trim($_REQUEST['rpassword'])) >= 6 && strlen(trim($_REQUEST['rpassword'])) <= 10
    ) {

        if ($_REQUEST['password'] != $_REQUEST['rpassword']) {
            $_SESSION['passwordMsg'] = "Password didn't match";
            unset($_REQUEST['password']);
            unset($_REQUEST['rpassword']);
        }

    } else {
        unset($_REQUEST['password']);
        unset($_REQUEST['rpassword']);
        $_SESSION['passwordMsg'] = "Password Length 6 to 10";
        $_SESSION['rpasswordMsg'] = "Repeated Password Length 6 to 10";
    }

// called store function for data saved
    if (!empty($_REQUEST['username']) && !empty($_REQUEST['email']) && !empty($_REQUEST['password'])) {
        $data = $storeObj->assign($_REQUEST)->store();
        if($data == 1){
            $_SESSION['msg'] = "Successfully Data Submitted";
            unset($_SESSION['usernameValue']);
            unset($_SESSION['emailValue']);
            header("location:index.php");
        }else{
            $_SESSION['msg'] = "Error While Data Submitted";
            header("location:index.php");
        }
    } else {
        header("location:create.php");
    }

} else {
    $_SESSION['tcMsg'] = "Check Terms & Condition";
    header('location: create.php');
}
