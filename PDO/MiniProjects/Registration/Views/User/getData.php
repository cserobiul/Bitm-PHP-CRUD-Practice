<?php
include_once('../../vendor/autoload.php');
$getDataObj = new \Apps\User\User();
include_once 'checkLogin.php';

$data = $getDataObj->index();

if(!empty($data)){
    echo json_encode($data);
}else{
    echo "No data in database";
}
