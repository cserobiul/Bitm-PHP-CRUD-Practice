<?php

include_once('../../vendor/autoload.php');
$dataObj = new \Apps\User\User();
include_once('layout/header.php');

if(isset($_SESSION['errMsg']) && !empty($_SESSION['errMsg'])){
    echo '<h4> <font color="red">'.$_SESSION['errMsg'].'</font> </h4>';
    unset($_SESSION['errMsg']);
    echo '<p align="center"><img src="../../img/404.png" class="img-responsive" align="middle"></p>';
    echo '<a href="index.php">HOME PAGE</a>';
    }else{
    header("location:index.php");
}
?>


<?php include_once('layout/footer.php'); ?>
