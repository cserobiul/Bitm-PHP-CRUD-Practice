<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                UMS</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php
                if(!isset($_SESSION['loginUser']) && empty($_SESSION['loginUser'])){
                    echo '<li><a href="login.php"> <span class="glyphicon glyphicon-log-in" aria-hidden="true"> </span> Login</a></li>';
                }
                ?>

                <?php
                if(isset($_SESSION['loginUser'])){
                    if($_SESSION['is_admin'] == 1){
                        echo '<li><a href="create.php"> <span class="glyphicon glyphicon-log-out" aria-hidden="true"> </span> Add New</a></li>';
                    }
                    echo '<li><a href="logout.php"> <span class="glyphicon glyphicon-log-out" aria-hidden="true"> </span> Logout</a></li>';
                    
                }
                ?>


            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>