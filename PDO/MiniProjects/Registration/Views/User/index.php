<?php
include_once('../../vendor/autoload.php');
$dataObj = new \Apps\User\User();
include_once('layout/header.php');
include_once 'checkLogin.php';
?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <strong>Dear !</strong>
        <?php
        echo ucwords($_SESSION['loginUser']).' You login as a ';
        if ($_SESSION['is_admin'] == 1) {
            echo '<strong>Admin</strong>';
        } else {
            echo '<strong>User</strong>';
        }

        ?>
    </div>
<?php
// show messages
if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {?>
    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <?php
        echo '<strong>'.ucwords($_SESSION['msg']).'</strong>';
        unset($_SESSION['msg']);
        ?>
    </div>
<?php } ?>

    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">
                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                All Active User List </h3>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr class="success">
                        <th><span class="glyphicon glyphicon-sort-by-attributes" aria-hidden="true"></span> Serial</th>
                        <th><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Username</th>
                        <th><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Email</th>
                        <th><span class="glyphicon glyphicon-stats" aria-hidden="true"></span> Status</th>
                        <th><span class="glyphicon glyphicon-sunglasses" aria-hidden="true"></span> User Type</th>
                        <th><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Join At</th>
                        <th colspan="3"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span> Action</th>
                    </tr>

                    </thead>
                    <tbody id="data-load-here">
                    <?php
                    $_REQUEST['userStatus'] = "active";
                    $data = $dataObj->assign($_REQUEST)->index();

                    if (!empty($data)) {
                        $sl = 1;
                        foreach ($data as $value) { ?>
                            <tr>
                                <td><?php echo $sl++; ?></td>
                                <td><?php echo ucwords($value['username']); ?></td>
                                <td><?php echo $value['email']; ?></td>
                                <td><?php
                                    if ($value['is_active'] == 1) {
                                        echo 'Active';
                                    } else {
                                        echo 'Deactive';
                                    }

                                    ?></td>
                                <td><?php
                                    if ($value['is_admin'] == 1) {
                                        echo 'Admin';
                                    } else {
                                        echo 'User';
                                    }

                                    ?></td>
                                <td><?php echo $value['created_at']; ?></td>
                                <td>
                                    <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                                    <a href="show.php?id=<?php echo $value['u_id']; ?>">View</a></td>
                                <td>
                                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                    <a href="#">Edit</a></td>
                                <td>
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                    <a href="#">Delete</a></td>
                            </tr>
                        <?php }
                    } else {
                        echo 'No User';
                    }

                    ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        //    function ucwords (str) {
        //        return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
        //            return $1.toUpperCase();
        //        });
        //    }
        //    $.ajax({
        //        type:"POST",
        //        data:"",
        //        url:"getData.php",
        //        success:function (result) {
        //            var resultObj = JSON.parse(result);
        //            var dataHandler = $("#data-load-here");
        //            $.each(resultObj,function (index,value) {
        //                var newRow = $("<tr>");newRow.html("<td>"+value.id+"</td><td>"+ucwords(value.username)+"</td> <td>"+value.email+"</td> <td>"+value.is_active+"</td><td>"+value.is_admin+"</td><td>"+value.created_at+"</td>");
        //                dataHandler.append(newRow);
        //                console.log(resultObj);
        //            })
        //        }
        //    })

    </script>

<?php include_once('layout/footer.php'); ?>