<?php
include_once('../../vendor/autoload.php');
$dataObj = new \Apps\User\User();
include_once('layout/header.php');
?>

<div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4">
    <form method="post" action="store.php">
        <div class="form-group">
            <label for="username">User Name</label>
            <input type="text" name="username" class="form-control" id="username"
                   value="<?php if (isset($_SESSION['usernameValue']) && !empty($_SESSION['usernameValue'])) {
                echo $_SESSION['usernameValue'];
                unset($_SESSION['usernameValue']);
            } ?>" maxlength="15" placeholder="Username">
            <p>
                <?php if (isset($_SESSION['usernameMsg']) && !empty($_SESSION['usernameMsg'])) {
                    echo '<font color="#b22222">' . $_SESSION['usernameMsg'] . '</font>';
                    unset($_SESSION['usernameMsg']);
                } ?>
            </p>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="text" name="email" class="form-control" id="exampleInputEmail1"
                   value="<?php if (isset($_SESSION['emailValue']) && !empty($_SESSION['emailValue'])) {
                       echo $_SESSION['emailValue'];
                       unset($_SESSION['emailValue']);
                   } ?>" maxlength="50" placeholder="Email">
            <p>
                <?php if (isset($_SESSION['emailMsg']) && !empty($_SESSION['emailMsg'])) {
                    echo '<font color="#b22222">' . $_SESSION['emailMsg'] . '</font>';
                    unset($_SESSION['emailMsg']);
                } ?>
            </p>
        </div>
        <?php if(isset($_SESSION['is_admin'])){
            if($_SESSION['is_admin'] == 1){ ?>
        <div class="form-group">
            <label for="userrole">User Role</label>
            <select name="userRole" class="form-control">
                <option value="user">User</option>
                <option value="admin">Admin</option>
            </select>
        </div>
        <div class="form-group">
            <label for="userstatus">User Status</label>
            <select name="userStatus" class="form-control">
                <option value="active">Active</option>
                <option value="deactive">Deactive</option>
            </select>
        </div>
        <?php }}  ?>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1"
                   maxlength="10" placeholder="Password">
            <p>
                <?php if (isset($_SESSION['passwordMsg']) && !empty($_SESSION['passwordMsg'])) {
                    echo '<font color="#b22222">' . $_SESSION['passwordMsg'] . '</font>';
                    unset($_SESSION['passwordMsg']);
                } ?>
            </p>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Repeate Password</label>
            <input type="password" name="rpassword" class="form-control" id="exampleInputPassword1"
                   maxlength="10" placeholder="Repeate Password">
            <p>
                <?php if (isset($_SESSION['rpasswordMsg']) && !empty($_SESSION['rpasswordMsg'])) {
                    echo '<font color="#b22222">' . $_SESSION['rpasswordMsg'] . '</font>';
                    unset($_SESSION['rpasswordMsg']);
                } ?>
            </p>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="tc" value="1"> Terms & Condition
                <p>
                    <?php if (isset($_SESSION['tcMsg']) && !empty($_SESSION['tcMsg'])) {
                        echo '<font color="#b22222">' . $_SESSION['tcMsg'] . '</font>';
                        unset($_SESSION['tcMsg']);
                    } ?>
                </p>
            </label>

        </div>
        <input type="submit" class="btn btn-default" value="Add User">
    </form>
</div>
<?php include_once('layout/footer.php'); ?>
